package pro.labapp.mobileappproject;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.security.MessageDigest;
import java.io.IOException;

class PostData extends AsyncTask<String, Void, Boolean> {

    private Data data;

    PostData(Data data) {
        this.data = data;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        hashID();
    }

    @Override
    protected Boolean doInBackground(String... params) {
        JSONObject request201 = createJSON201();
        JSONObject request202 = createJSON202();
        JSONObject request203 = createJSON203();
        JSONObject request204 = createJSON204();

        postJSON(request201);
        postJSON(request202);
        postJSON(request203);
        postJSON(request204);

        return true;
    }

    protected void onPostExecute(Boolean result) {

    }

    private void postJSON(JSONObject request) {
        HttpPost post = new HttpPost(
                "http://195.201.111.238:8001/data");

        HttpClient client = new DefaultHttpClient();

        // post.setEntity(new StringEntity(request.toString(), "utf-8"));
        StringEntity entity = new StringEntity(request.toString(), HTTP.UTF_8);
        entity.setContentType("application/json");
        post.setHeader("Content-Type", "application/json");
        post.setHeader("Accept", "application/json");
        post.setEntity(entity);
        Log.d("PostData", request.toString());
        try {
            HttpResponse response = client.execute(post);
            Log.d("PostData", response.toString());
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("PostData", "Failed");
        }
    }

    private JSONObject createJSON201() {
        JSONObject source201 = new JSONObject();
        try {
            source201.put("id_device", hashID());
            source201.put("source", 201);

            // ‘value’: [{‘x’: 1, ‘y’: 256, ‘time’ : ‘1415463675’}, {‘x’: 10, ‘y’: 256, ‘time’ : ‘1415463678’}]
            //{
            // "id_device":"d3d9446802a44259755d38e6d163e820",
            // "source":201,
            // "values":[{"longitude":55,"latitude":55.3,"time":1415463678},{"longitude":56,"latitude":56.3,"time":1415463679}]
            // }
            JSONArray values = new JSONArray();
            for(int i = 0; i < data.arraySize; i++) {
                JSONObject valueObj = new JSONObject()
                        .put("longitude", (float)data.longitude[i])
                        .put("latitude", (float)data.latitude[i])
                        .put("velocity", (float)data.speed[i])
                        .put("height", (float)data.altitude[i])
                        .put("time", (float)data.time[i]);
                values.put(valueObj);
            }
            source201.put("value", values);
            return source201;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    private JSONObject createJSON202() {
        JSONObject source202 = new JSONObject();
        try {
            source202.put("id_device", hashID());
            source202.put("source", 202);
            JSONArray values = new JSONArray();
            for(int i = 0; i < data.arraySize; i++) {
                JSONObject valueObj = new JSONObject()
                        .put("power", data.batteryLevel[i])
                        .put("time", data.time[i]);
                values.put(valueObj);
            }
            source202.put("value", values);
            return source202;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    private JSONObject createJSON203() {
        JSONObject source203 = new JSONObject();
        try {
            source203.put("id_device", hashID());
            source203.put("source", 203);
            JSONArray values = new JSONArray();
            for(int i = 0; i < data.arraySize; i++) {
                JSONObject valueObj = new JSONObject()
                        .put("temp", data.cpuTemperature[i])
                        .put("time", data.time[i]);
                values.put(valueObj);
            }
            source203.put("value", values);
            return source203;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    private JSONObject createJSON204() {
        JSONObject source203 = new JSONObject();
        try {
            source203.put("id_device", hashID());
            source203.put("source", 204);
            JSONArray values = new JSONArray();
            for(int i = 0; i < data.arraySize; i++) {
                JSONObject valueObj = new JSONObject()
                        .put("light", data.light[i])
                        .put("time", data.time[i]);
                values.put(valueObj);
            }
            source203.put("value", values);
            return source203;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    private String hashID() {
        try {
            return HexMD5ForString(String.valueOf(data.id));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    String HexForByte(byte b) {
        String Hex = Integer.toHexString((int) b & 0xff);
        boolean hasTwoDigits = (2 == Hex.length());
        if(hasTwoDigits) return Hex;
        else return "0" + Hex;
    }

    String HexForBytes(byte[] bytes) {
        StringBuffer sb = new StringBuffer();
        for(byte b : bytes) sb.append(HexForByte(b));
        return sb.toString();
    }

    String HexMD5ForString(String text) throws Exception {
        MessageDigest md5 = MessageDigest.getInstance("MD5");
        byte[] digest = md5.digest(text.getBytes());
        return HexForBytes(digest);
    }
}