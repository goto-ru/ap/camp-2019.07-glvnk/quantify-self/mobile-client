package pro.labapp.mobileappproject;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

import com.basel.DualButton.DualButton;

import java.util.concurrent.ExecutionException;

public class HomeActivity extends AppCompatActivity {

    private static final int PERMISSION_REQUEST_CODE = 1;
    //    Button buGetCode;
    TextView codeText;

    int authCode;
    int id_device;
    int name = 1231;
    Boolean haveCode;
    private final String authURL = "http://195.201.111.238:8001/2fa?id_device="
            + id_device
            + "&name="
            + name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        codeText = (TextView) findViewById(R.id.codeText);

//        buGetCode = (Button) findViewById(R.id.buGetCode);
//        buGetCode.setOnClickListener(getCodeListener);
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission
                .ACCESS_FINE_LOCATION, Manifest.permission
                .INTERNET, Manifest.permission
                .RECEIVE_BOOT_COMPLETED, Manifest.permission
                .READ_PHONE_STATE, Manifest.permission
                .ACCESS_NETWORK_STATE, Manifest.permission
                .READ_EXTERNAL_STORAGE, Manifest.permission
                .WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);

        id_device = getDeviceId();

        startService(new Intent(this, Background.class));
        DualButton dualBtn = findViewById(R.id.dualBtn);
        dualBtn.setDualClickListener(new DualButton.OnDualClickListener() {
            public void onClickFirst(Button btn) {
                getAuthCode();
            }
            public void onClickSecond(Button btn) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://google.com")));
            }
        });
    }

    private void onStartActivity() {
        Intent intent = new Intent(HomeActivity.this, WebView.class);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    private void getAuthCode() {
        AuthCode authCode = new AuthCode();
        try {
            haveCode = authCode.execute(authURL).get();
            String result = authCode.result;
            if (haveCode) {
                Log.e("RESULT", result);
                parseAuthCode(result);
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    @SuppressLint("SetTextI18n")
    private void parseAuthCode(String serverResponse) {
        //{"code": "38552", "expiration": 1563965505.5080793}
        try {
            authCode = Integer.parseInt(serverResponse.split("\",")[0].split(": \"")[1]);
            Log.e("CODE", String.valueOf(authCode));
            codeText.setText("Твой код для авторизации: "+ String.valueOf(authCode));
        } catch( Exception e) {
            e.printStackTrace();
        }
    }

    private int getDeviceId() {
        //Код частично со StackOverflow
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            return 0;
        }

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            String imeiID = telephonyManager.getImei();
            return (int)Long.parseLong(imeiID);
        }
        else {
            String phoneID = telephonyManager.getDeviceId();
            return (int)Long.parseLong(phoneID);
        }
    }
}