package pro.labapp.mobileappproject;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

class AuthCode extends AsyncTask<String, Void, Boolean> {

    public static final String REQUEST_METHOD = "GET";
    public static final int READ_TIMEOUT = 15000;
    public static final int CONNECTION_TIMEOUT = 15000;

    public String result;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected Boolean doInBackground(String... params) {
        String stringUrl = params[0];
        try {
            result = connect(stringUrl);
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
            result = null;
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            result = null;
            return false;
        }
        return true;
    }

    protected void onPostExecute(Boolean result) {

    }

    private String connect(String url) throws IOException {
        //Create a URL object holding our url
        URL Url = new URL(url);         //Create a connection
        HttpURLConnection connection =(HttpURLConnection)
                Url.openConnection();
        Log.e("CONNECTION", "Connection opened");
        //Set methods and timeouts
        connection.setRequestMethod(REQUEST_METHOD);
        connection.setReadTimeout(READ_TIMEOUT);
        connection.setConnectTimeout(CONNECTION_TIMEOUT);

        //Connect to our url
        connection.connect();
        Log.e("CONNECTION", "Connected");
        return readData(connection.getInputStream());
    }

    private String readData(InputStream stream) throws IOException {
        InputStreamReader streamReader = new
                InputStreamReader(stream);         //Create a new buffered reader and String Builder
        BufferedReader reader = new BufferedReader(streamReader);
        StringBuilder stringBuilder = new StringBuilder();         //Check if the line we are reading is not null
        String inputLine = "";

        while((inputLine = reader.readLine()) != null){
            stringBuilder.append(inputLine);
        }         //Close our InputStream and Buffered reader
        Log.e("CONNECTION", stringBuilder.toString());
        reader.close();
        streamReader.close();         //Set our result equal to our stringBuilder
        return stringBuilder.toString();
    }
}