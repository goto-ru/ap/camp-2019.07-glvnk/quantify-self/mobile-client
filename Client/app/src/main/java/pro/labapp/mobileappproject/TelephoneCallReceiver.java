package pro.labapp.mobileappproject;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.util.Log;

public class TelephoneCallReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(final Context context, Intent intent) {
        String incomingCallNumber = intent.getExtras().getString(TelephonyManager.EXTRA_INCOMING_NUMBER);
        Log.e("Call", incomingCallNumber);
    }

}
