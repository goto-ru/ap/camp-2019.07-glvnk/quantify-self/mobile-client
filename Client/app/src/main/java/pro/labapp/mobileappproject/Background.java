package pro.labapp.mobileappproject;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.BatteryManager;
import android.os.Binder;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

public class Background extends Service {

    private final IBinder binder = new LocalBinder();

    private Data data;
    private int dataSize = 2;
    private int dataIteration = 0;

    private Timer updateTimer;
    private LocationManager locationManager;

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @SuppressLint("StaticFieldLeak")
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        data = new Data();
        //data.arraySize = dataSize;
        prepareToWork();
        startTimer();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "Служба остановлена",
                Toast.LENGTH_SHORT).show();
    }

    private void startTimer() {
        updateTimer = new Timer();
        UpdateInfoTimerTask updateInfoTask = new UpdateInfoTimerTask();
        updateTimer.schedule(updateInfoTask, 1, 6000); //period on prod = 300000
    }

    private void prepareToWork() {
//        preferences = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);

        Toast.makeText(this, "Служба запущена",
                Toast.LENGTH_SHORT).show();

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        SensorManager sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

        Sensor lightSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        if (lightSensor != null) {
            sensorManager.registerListener(
                    lightSensorListener, lightSensor, SensorManager.SENSOR_DELAY_NORMAL);
        }

        Sensor walkSensor = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
        if (walkSensor != null) {
            sensorManager.registerListener(
                    walkSensorListener, walkSensor, SensorManager.SENSOR_DELAY_UI);
        }
    }

    private void getAllCurrentData() {
        if(dataIteration >= dataSize) {
            return;
        }
        updateBatteryLevelInfo();
        updateCpuTemperatureInfo();
        updateWorkTimeInfo();
        updateDeviceId();
        updateTimeInfo();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        updateLocationInfo(locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER));

        if(data.steps[dataIteration] == 0 && dataIteration > 0) {
            data.steps[dataIteration] = data.steps[dataIteration - 1];
        }

        dataIteration++;
    }

    private void updateLocationInfo(Location location) {
        if(location == null) {
            return;
        }
        data.latitude[dataIteration] = location.getLatitude();
        data.longitude[dataIteration] = location.getLongitude();
        data.altitude[dataIteration] = location.getAltitude() * 0.01; //Перевод в метры
        data.speed[dataIteration] = location.getSpeed();
    }

    private void updateTimeInfo() {
       Calendar calendar = Calendar.getInstance();
        data.time[dataIteration] = calendar.getTimeInMillis();
    }

    private void updateWorkTimeInfo() {
        data.workTime[dataIteration] = SystemClock.elapsedRealtime();
    }

    private void updateBatteryLevelInfo() {
        //Код со StackOverflow
        Intent batteryIntent = registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        int level = batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
        float result = 0;
        // Error checking that probably isn't needed but I added just in case.
        if(level == -1 || scale == -1) {
            result = 50.0f;
        }
        else {
            result = ((float)level / (float)scale) * 100.0f;
        }
        data.batteryLevel[dataIteration] = result;
    }

    private void updateCpuTemperatureInfo() {
        //Код со StackOverflow
        Process process;
        float result;
        try {
            process = Runtime.getRuntime().exec("cat sys/class/thermal/thermal_zone0/temp");
            process.waitFor();
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line = reader.readLine();
            if(line!=null) {
                float temp = Float.parseFloat(line);
                result = temp / 1000.0f;
            }else{
                result =  51.0f;
            }
        } catch (Exception e) {
            e.printStackTrace();
            result =  0.0f;
        }
        data.cpuTemperature[dataIteration] = result;
    }

    private void updateDeviceId() {
        //Код частично со StackOverflow
        long id;
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            String imeiID = telephonyManager.getImei();
            id = Long.parseLong(imeiID);
        }
        else {
            String phoneID = telephonyManager.getDeviceId();
            id = Long.parseLong(phoneID);
        }
        data.id[dataIteration] = id;
    }

    private final SensorEventListener walkSensorListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
            data.steps[dataIteration] = (int) event.values[0];
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
    };

    private final SensorEventListener lightSensorListener = new SensorEventListener() {

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
            //Auto-generated method stub
        }

        @Override
        public void onSensorChanged(SensorEvent event) {
            if (event.sensor.getType() == Sensor.TYPE_LIGHT) {
                if (data != null) {
                    data.light[dataIteration] = event.values[0];
                }
            }
        }

    };

    public class LocalBinder extends Binder {
        public Background getService() {
            return Background.this;
        }
    }

    class UpdateInfoTimerTask extends TimerTask {
        @Override
        public void run() {
            getAllCurrentData();
            if(dataIteration == dataSize) {
                dataIteration = 0;
                postData();
            }
        }
    }

    private void postData() {
        Log.d("BACKGROUND", "post data");
        data.showDebugInfo();
        PostData post = new PostData(data);
        post.execute();
        dataIteration = 0;
    }
}
