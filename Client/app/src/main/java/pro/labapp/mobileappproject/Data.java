package pro.labapp.mobileappproject;

import android.util.Log;

public class Data {
    public float[] speed = {0, 0}; //Скорость
    public float[] light = {0, 0}; //Освещенность
    public float[] cpuTemperature = {0, 0}; //Температура процессора
    public float[] batteryLevel = {0, 0}; //Заряд батареи
    public double[] altitude = {0, 0}; //Высота
    public double[] latitude = {0, 0}; //Широта
    public double[] longitude = {0, 0}; //Долгота
    public long[] time = {0, 0}; //Время измерения
    public long[] workTime = {0, 0}; //Время работы устройства
    public long[] id = {0, 0}; //id
    public int[] steps = {0, 0}; //Шаги

    public final int arraySize = 2;

    public void showDebugInfo() {
        String logs =
                "Battery = " + "{ " + batteryLevel[0] + ", " + batteryLevel[1] + " }\n"  +
                "CPU temperature = " + "{ " + cpuTemperature[0] + ", " + cpuTemperature[1] + " }\n" +
                "Light = " + "{ " +  light[0] + ", " + light[1] + " }\n" +
                "Work Time = " + "{ " + workTime[0] + ", " + workTime[1] + " }\n" +
                "ID = " + "{ " + id[0] + ", " + id[1] + " }\n" +
                "Time = " + "{ " + time[0] + ", " + time[1] + " }\n" +
                "Longitude = " + "{ " + longitude[0] + ", " + longitude[1] + " }\n" +
                "Latitude = " + "{ " + latitude[0] + ", " + latitude[1] + " }\n" +
                "Altitude = " + "{ " + altitude[0] + ", " + altitude[1] + " }\n" +
                "Speed = " + "{ " + speed[0] + ", " + speed[1] + " }\n" +
                "Steps = " + "{ " + steps[0] + ", " + steps[1] + " }\n";
        Log.d("DATA\n", logs);
    }
}
